/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.util.List;


/**
 *
 * @author 65160
 */
public class ArtistService {

    public List<ArtistReport> getArtistByTotalPrice(int limit) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(limit);
    }
    
    public List<ArtistReport> getArtistByTotalPrice(String begin, String end) {
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByTotalPrice(begin, end, 10);
    }
}
